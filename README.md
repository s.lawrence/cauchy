# Contour deformations for sign problems

## Papers

If using this code, or code derived from it, it may be appropriate to cite one or more of the following papers.

- [Normalizing Flows and the Real-Time Sign Problem](https://arxiv.org/abs/2101.05755)
- [Lattice scalar field theory at complex coupling](https://arxiv.org/abs/2205.12303)

## Requirements

The code in this repository assumes a reasonably up-to-date python 3. The main packages used are `jax` and `flax`. The required packages are listed in detail in `requirements.txt`. It is probably easiest to install all requirements using `venv`, as follows:

```
python -m venv env
source env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Workflow

First generate a physical model description with `mkmodel.py`. Then, with
either `contour.py` or `nf.py`, generate and train a neural network to solve
the sign problem. If using a contour, collect data with `sample.py`; if using a
normalizing flow, collect data with `gaussian.py`. Finally, invoke
`boostrap.py` to obtain estimates and error bars.

Here's an example session that uses a contour deformation:
```
mkdir -p data/
./mkmodel.py data/model.pickle scalar '()' 6 0 0.5 0.
./contour.py data/model.pickle data/contour.pickle # Terminate with CTRL-C
./sample.py data/model.pickle data/contour.pickle # Terminate with CTRL-C
./bootstrap.py < data/samples.dat
```

And here's an example using a normalizing flow:
```
mkdir -p data/
./mkmodel.py data/model.pickle scalar '()' 6 0 0.5 0.
./nf.py data/model.pickle data/nf.pickle # Terminate with CTRL-C
./gaussian.py data/model.pickle data/nf.pickle # Terminate with CTRL-C
./bootstrap.py < data/samples.dat
```

## Conventions

Most code (whether training or sampling) will continue to run until a stop is
requested via CTRL-C.

## Organization

The main scripts are:

- **`bootstrap.py`**: Computes estimates and errorbars, from samples
- **`contour.py`**: Trains a contour
- **`gaussian.py`**: Samples from a normalizing flow
- **`info.py`**: Prints information on a pickle file
- **`mkmodel.py`**: Defines a model
- **`nf.py`**: Trains a normalizing flow
- **`sample.py`**: Samples from a contour

The directory structure is:

- **`comparison/`**: Computations via other methods
- **`ex/`**: Various (typically older) scripts
- **`mc/`**: Various Monte Carlo methods
- **`models/`**: Physical model definitions
- **`notes/`**: Notes
- **`pub/`**: Publications

## Models

The following models are implemented:

- Scalar field theory
- Thirring model
- Hubbard model

## Tricks

Set `JAX_DEBUG_NANS=True` in the environment to catch `nan`-related errors early.

If long lists of options are getting unwieldy, you can put them in a file and
include that file in the command line via `@`, as in `./contour.py
@options.txt`.

