# Contour

- HMC
- Adiabatic training
- automated re-initialization in contour.py


# Normalizing flows

- Adiabatic paths for S-K angle
- Make adiabatic training more manageable
- Make adiabatic paths for negative coupling, negative temperature
- Other loss functions?


# General

- Hamiltonian limit (time-like spacing < 1)
- Switch to flax serialization (for compatibility?)
- Neural networks for which Jacobian is either trivial or linear-time computable
- In other cases, compute determinant of each linear part only once
- customizable NN width
- use argparse.FileType
- OTOC


