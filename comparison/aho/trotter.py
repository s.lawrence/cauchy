#!/usr/bin/env python

import numpy as np
import sys

contour = 'S'

m2 = float(sys.argv[1])
g = complex(sys.argv[2])
beta = int(sys.argv[3])
time = int(sys.argv[4])
N = int(sys.argv[5])

a = np.zeros((N,N))
for i in range(N-1):
    a[i,i+1] = np.sqrt(i+1)
adag = a.transpose().conjugate()

x = 1/(np.sqrt(2)) * (a + adag)
p = 1j*np.sqrt(1/2) * (adag - a)
Hx = 0.5*m2*x@x + g*x@x@x@x
Hp = 0.5*p@p

Ex, Ux = np.linalg.eig(Hx)
Ep, Up = np.linalg.eig(Hp)

# Temporal transfer matrix
transfer_t_x_2 = Ux @ np.diag(np.exp(-Ex/2)) @ np.linalg.inv(Ux)
transfer_t_p = Up @ np.diag(np.exp(-Ep)) @ np.linalg.inv(Up)
transfer_t = transfer_t_x_2 @ transfer_t_p @ transfer_t_x_2

# Timelike transfer matrix, forward and backword
transfer_f_x_2 = Ux @ np.diag(np.exp(-1j*Ex/2)) @ np.linalg.inv(Ux)
transfer_f_p = Up @ np.diag(np.exp(-1j*Ep)) @ np.linalg.inv(Up)
transfer_f = transfer_f_x_2 @ transfer_f_p @ transfer_f_x_2
transfer_b = transfer_f.transpose().conjugate()

if contour == 'L':
    rho = np.eye(N)
    for _ in range(beta):
        rho = rho @ transfer_t
    Zb = rho.trace()

    # Thermal partition function
    print(f'Thermal partition function: {Zb}')

    # Correlator
    for n in range(time+1):
        T = rho
        for m1 in range(n):
            T = T @ transfer_b
        T = T @ x
        for m2 in range(n):
            T = T @ transfer_f
        T = T @ x
        print(T.trace()/Zb)

elif contour == 'S':
    beta1 = int(beta/2)
    beta2 = beta-beta1

    rho1, rho2 = np.eye(N), np.eye(N)
    for _ in range(beta1):
        rho1 = rho1 @ transfer_t
    for _ in range(beta2):
        rho2 = rho2 @ transfer_t

    # Correlator
    for n in range(time+1):
        T = np.eye(N)
        T_ = np.eye(N)

        T_ = x @ T_

        for m1 in range(time):
            if n == m1:
                T_ = x @ T_
            T = transfer_f @ T
            T_ = transfer_f @ T_
        if n == time:
            T_ = x @ T_

        T = rho1 @ T
        T_ = rho1 @ T_

        for m2 in range(time):
            T = transfer_b @ T
            T_ = transfer_b @ T_

        T = rho2 @ T
        T_ = rho2 @ T_
        print(T_.trace()/T.trace())

