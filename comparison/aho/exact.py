#!/usr/bin/env python

import numpy as np
import sys

m2 = complex(sys.argv[1])
g = complex(sys.argv[2])
beta = float(sys.argv[3])
N = 300

a = np.zeros((N,N))
for i in range(N-1):
    a[i,i+1] = np.sqrt(i+1)
adag = a.transpose().conjugate()

x = 1/(np.sqrt(2)) * (a + adag)
p = 1j*np.sqrt(1/2) * (adag - a)
H = 0.5*m2*x@x + 0.5*p@p + g*x@x@x@x

E, U = np.linalg.eig(H)
rho = U @ np.diag(np.exp(-beta*E)) @ np.linalg.inv(U)

print(rho.trace())
print((rho@x@x).trace()/rho.trace())

