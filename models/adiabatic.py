import jax.numpy as jnp

class Trivial:
    def action(self, phi):
        return jnp.sum(phi*phi)/2.

    def observe(self, phi):
            return jnp.array([self.action(phi)])

class TrivialBlend:
    def __init__(self, model, alpha):
        self.trivial = Trivial()
        self.model = model
        self.lattice = model.lattice
        self.alpha = alpha

    def action(self, phi):
        return self.model.action(phi)*self.alpha + (1-self.alpha)*self.trivial.action(phi)

    def observe(self, phi):
        return jnp.array([self.action(phi)])

