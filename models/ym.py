from dataclasses import dataclass
from typing import Tuple

import jax.numpy as jnp
import numpy as np

@dataclass
class Lattice:
    L: int
    beta: int
    def __post_init__(self):
        self.V = self.L * self.beta
        self.dof = 4*self.V

    def idx(self,t,x):
        return (t%self.beta)*self.L + (x%self.L)

