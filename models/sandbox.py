from dataclasses import dataclass

import jax.numpy as jnp

@dataclass
class Lattice:
    dof: int = 2
    periodic_contour: bool = True

@dataclass
class Model:
    lattice: Lattice = Lattice()

    def action(self, z):
        return jnp.sum(-jnp.log(0.5+jnp.cos(z+0j)))

    def observe(self, z):
        return []
