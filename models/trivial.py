from dataclasses import dataclass

import jax.numpy as jnp
import numpy as np

def sk_contour(nbeta, nt, shape='S'):
    nbeta1 = int(nbeta/2)
    nbeta2 = nbeta-nbeta1
    return np.concatenate([
        1j*np.ones(nt),
        np.ones(nbeta1),
        -1j*np.ones(nt),
        np.ones(nbeta2),
        ])

@dataclass
class Model:
    def __post_init__(self):
        self.dof = 6
        self.D = 0
        self.NT = 6
        self.geom = ()
        self.shape = (6,)
        self.nbeta = 2
        self.nt = 2
        self.contour = sk_contour(self.nbeta, self.nt)
        self.contour_site = np.array(
                [(self.contour[i]+self.contour[(i-1)%self.NT])/2 for i in range(self.NT)])
        self.dt_link = np.tile(self.contour.reshape((self.NT,)+(1,)*self.D), (1,)+self.geom)
        self.dt_site = np.tile(self.contour_site.reshape((self.NT,)+(1,)*self.D), (1,)+self.geom)
        print(self.dt_site)
        self.lamda = 1.

    def action(self, z, t=1.):
        quartic = z[0]**4 + z[1]**4 + z[2]**4 + z[3]**4 + z[4]**4 + z[5]**4
        mass = 0.15 * (0.)
        kinetic = 0.
        return quartic + mass + kinetic

    def observe(self, z):
        return 0.

