from dataclasses import dataclass
from typing import Tuple

import jax.numpy as jnp

@dataclass
class Lattice:
    dof: int

    def __post_init__(self):
        self.periodic_contour=False

@dataclass
class Model:
    lattice: Lattice
    m: complex

    def action(self, phi):
        return jnp.sum(self.m*phi*phi)

    def observe(self, phi):
        return jnp.array([phi[0]*phi[i] for i in range(self.lattice.NT)])