#!/usr/bin/env python

import argparse
from functools import partial
import pickle
import sys
import time

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np

from mc import metropolis
from models import scalar
import nf

# TODO somehow read layers from the serialized file

parser = argparse.ArgumentParser(
        description="Sample from normalising flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
        )
parser.add_argument('model', type=str, help="model filename")
parser.add_argument('nf', type=str, help="flow filename")
parser.add_argument('--debug-nan', action='store_true',
        help='nan debugging mode')
parser.add_argument('--debug-inf', action='store_true',
        help='inf debugging mode')
parser.add_argument('--no-jit', action='store_true',
        help='disable JIT compilation')
parser.add_argument('-l', '--layers', type=int, default=-1,
        help='number of (hidden) layers')
parser.add_argument('--metropolis', action='store_true', help="sample using Metropolis")
parser.add_argument('-B', '--batches', default=-1, type=int,
        help='number of batches before termination')
parser.add_argument('-S', '--samples', default=1<<15, type=int,
        help='number of samples per batch')
parser.add_argument('--x64', action='store_true',
        help='64-bit mode')
parser.add_argument('--seed', type=int, help="random seed")
args = parser.parse_args()

if args.debug_nan:
    jax.config.update("jax_debug_nans", True)
if args.debug_inf:
    jax.config.update("jax_debug_infs", True)
if args.no_jit:
    jax.config.update("jax_disable_jit", True)
if args.x64:
    jax.config.update("jax_enable_x64", True)

with open(args.model, 'rb') as f:
    model = eval(f.read())
flow, contour = nf.make(jax.random.PRNGKey(0), args.layers, model)
flow, contour = eqx.tree_deserialise_leaves(args.nf, (flow, contour))

if args.seed is None:
    seed = time.time_ns()
else:
    seed = args.seed
sampleKey = jax.random.PRNGKey(seed)

if args.metropolis:
    @jax.jit
    def Seff(x):
        phi, ld = flow(x)
        S = model.action(phi) - ld
        return S.real
    chain = metropolis.Chain(Seff, jnp.zeros(model.dof), sampleKey)
    for x in chain.iter(1000):
        phi, _ = flow(x)
        print(f'{Seff(x):8.4}    {np.linalg.norm(x):8.4}   | {jnp.linalg.norm(phi):8.4}')
else:
    @jax.jit
    def sample(key):
        skey, key = jax.random.split(key)
        x = jax.random.normal(key, (args.samples,model.dof))
        y, fld = jax.vmap(flow)(x)
        phi, cld = jax.vmap(contour)(y)
        ld = fld + cld
        S = jax.vmap(model.action)(phi) - ld
        logboltz = jnp.sum(x**2,axis=-1)/2-S + jnp.log(2*jnp.pi)*(x.shape[-1]/2)
        boltz = jnp.exp(logboltz)
        # Observables, inverse average sign and the loss value
        obss = jax.vmap(model.observe)(phi)
        obssboltz = jnp.einsum('ij,i->j',obss,boltz)
        obs = obssboltz / jnp.sum(boltz)
        sign = jnp.exp(1j*S.imag)
        invsignboltz = jnp.sum(sign*boltz)/jnp.sum(boltz)
        return key, jnp.mean(boltz), invsignboltz, *obs

    try:
        nbatch = 0
        while args.batches < 0 or nbatch < args.batches:
            nbatch += 1
            sampleKey, mboltz, invsignboltz, *obs = sample(sampleKey)
            print(mboltz, invsignboltz, *obs, flush=True)
    except KeyboardInterrupt:
        pass
