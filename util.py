import equinox as eqx
import jax
import jax.numpy as jnp

def lu_mul(LU, x):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ (U @ x)

def lu_mat(LU):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ U

def ones_zeros(m, N):
    # Returns an array which is m 1s followed by (N-m) zeros.
    j = jnp.arange(N)
    return jnp.int32(j < m)

def leaky_tanh(x):
    return 0.9*jnp.tanh(x) + 0.1*x

def leaky_celu(x):
    return jax.nn.celu(x) + 0.2*x

def normalize_quartic(x):
    """ Performs a change of variables to take e^(-x^2/2) to e^(-x^4). """
    z = x / jnp.sqrt(13. + jnp.abs(x))
    return (2.62712*z + 4.88342*z**3) / (1 + 3.41333 * z**2)
    #return x*(.704357 - .0671045*x**2 + .00564013*x**4)

    #return x * (0.721999 + 0.176603 * x**2 -0.00267861 * x**4) / (1 + 0.401437*x**2)
    # TODO eventually NANs come out of the one below. Use pade maybe?
    #return x*(0.717636 -0.0953717*x**2 + 0.0201631*x**4 -0.00243039*x**6 + 0.000113778*x**8)

def zero_mlp(mlp):
    def where(mlp):
        return [mlp.layers[-1].weight, mlp.layers[-1].bias]
    return eqx.tree_at(where, mlp, replace_fn = jnp.zeros_like)
