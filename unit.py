#!/usr/bin/env python

import unittest

import jax
import jax.numpy as jnp

from nf import *
from econtour import *

class TestFlowJacobians(unittest.TestCase):
    def test_LUFlow(self):
        kf, kx = jax.random.split(jax.random.PRNGKey(0))
        flow = LUFlow(kf, 6, 2)
        x = jax.random.normal(kx, (6,))
        _, ld = flow(x)
        jac = jax.jacfwd(lambda x: flow(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet)

    def test_RealNVP(self):
        kf, kx = jax.random.split(jax.random.PRNGKey(0))
        flow = RealNVP(kf, 6, 2)
        x = jax.random.normal(kx, (6,))
        _, ld = flow(x)
        jac = jax.jacfwd(lambda x: flow(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet, places=5)

    def test_Sigmoidal(self):
        kf, kx = jax.random.split(jax.random.PRNGKey(0))
        N = 6
        flow = Sigmoidal(kf, N, modes=3)
        x = jax.random.normal(kx, (N,))
        _, ld = flow(x)
        jac = jax.jacfwd(lambda x: flow(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet, places=5)

class TestContourJacobians(unittest.TestCase):
    def test_MLPContour(self):
        kc, kx = jax.random.split(jax.random.PRNGKey(0))
        N = 6
        contour = MLPContour(N=6, key=kc)
        x = jax.random.normal(kx, (N,))
        _, ld = contour(x)
        jac = jax.jacfwd(lambda x: contour(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet, places=5)

    def test_LUContour(self):
        kc, kx = jax.random.split(jax.random.PRNGKey(0))
        N = 6
        contour = LUContour(N=6, key=kc)
        x = jax.random.normal(kx, (N,))
        _, ld = contour(x)
        jac = jax.jacfwd(lambda x: contour(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet, places=5)

    def test_DiagonalContour(self):
        kc, kx = jax.random.split(jax.random.PRNGKey(0))
        N = 6
        contour = DiagonalContour(N=6, key=kc)
        x = jax.random.normal(kx, (N,))
        _, ld = contour(x)
        jac = jax.jacfwd(lambda x: contour(x)[0])(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        logdet = logdet + jnp.log(phase)
        self.assertAlmostEqual(ld, logdet, places=5)

if __name__ == '__main__':
    jax.config.update('jax_platform_name', 'cpu')
    unittest.main()

