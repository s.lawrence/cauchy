#!/usr/bin/env python

"""
TODO

implement all the contours
use these contour definitions in nf.py
"""

import json
import time

import argparse
import equinox as eqx
import jax
import jax.numpy as jnp
import jax.random as jr
import optax

import mcmc
from models import scalar
from util import lu_mul

class RealPlane(eqx.Module):
    def __init__(self, *, key=None):
        pass

    def __call__(self, x):
        return x, 0.

class LinearContour(eqx.Module):
    im: jnp.array

    def __init__(self, N, *, key=None):
        self.im = jnp.zeros((N,N))

    def __call__(self, x):
        N = len(x)
        mat = jnp.eye(N) + 1j*self.im
        s, ld = jnp.linalg.slogdet(mat)
        return mat @ x, ld + jnp.log(s)

class MLPContour(eqx.Module):
    mlp: eqx.nn.MLP
    fin: jnp.array

    def __init__(self, N, width_scale=1, depth=2, *, key):
        self.mlp = eqx.nn.MLP(
                in_size = N,
                out_size = 2*N,
                width_size = 2*N*width_scale,
                depth = depth,
                activation = jax.nn.relu,
                final_activation = jnp.tanh,
                key = key
        )
        self.fin = jnp.zeros((2*N,2*N))

    def deform(self, x):
        ri = self.fin @ self.mlp(x)
        return x + ri[::2] + 1j*ri[1::2]

    def __call__(self, x):
        jac = jax.jacfwd(self.deform)(x)
        s, ld = jnp.linalg.slogdet(jac)
        return self.deform(x), ld + jnp.log(s)

class LUContour(eqx.Module):
    re_lu: jnp.array
    im_lu: jnp.array

    def __init__(self, N, *, key=None):
        if key is None:
            self.re_lu = jnp.eye(N)
            self.im_lu = jnp.zeros((N,N))
        else:
            kr, ki = jr.split(key)
            self.re_lu = jnp.eye(N) + jr.uniform(kr, (N,N), minval=-0.1, maxval=0.1)
            self.im_lu = jr.uniform(ki, (N,N), minval=-0.1, maxval=0.1)

    def __call__(self, x):
        lu = self.re_lu + 1j * self.im_lu
        return lu_mul(lu, x), jnp.sum(jnp.log(jnp.diagonal(lu)))

class DiagonalContour(eqx.Module):
    rot_lu: jnp.array
    a: jnp.array
    b: jnp.array

    def __init__(self, N, *, key=None):
        self.rot_lu = jnp.eye(N)
        if key is None:
            self.a = jnp.zeros(N)
            self.b = jnp.ones(N)
        else:
            ka, kb = jr.split(key)
            self.a = jr.uniform(ka, (N,), minval=-0.1, maxval=0.1)
            self.b = jnp.ones(N)

    def __call__(self, x):
        # Rotate the real plane
        x = lu_mul(self.rot_lu, x)
        rot_ld = jnp.sum(jnp.log(jnp.diagonal(self.rot_lu)))
        y = self.a*jnp.tanh(self.b*x)
        dy = self.a*self.b*(1 - jnp.tanh(self.b*x)**2)
        dz = 1+1j*dy
        return x+1j*y, rot_ld + jnp.sum(jnp.log(dz))

class SequentialContour(eqx.Module):
    def __init__(self, *, key):
        pass

    def __call__(self, x):
        pass

def make(args, *, key=None):
    if key is None:
        key = jax.random.PRNGKey(0)
    #return MLPContour(args._dof, width_scale=args.width, depth=args.depth, key=key)
    return DiagonalContour(args._dof)
    #return LinearContour(args._dof)
    #return LUContour(args._dof)

def save(filename, args, contour):
    with open(filename, "wb") as f:
        args_str = json.dumps(vars(args))
        f.write((args_str + "\n").encode())
        eqx.tree_serialise_leaves(f, contour)

def load(filename):
    with open(filename, "rb") as f:
        args = json.loads(f.readline().decode())
        args = argparse.Namespace(**args)
        contour = make(args)
        return eqx.tree_deserialise_leaves(f, contour)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Train contour",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('--debug-nan', action='store_true',
            help='nan debugging mode')
    parser.add_argument('--debug-inf', action='store_true',
            help='inf debugging mode')
    parser.add_argument('--no-jit', action='store_true',
            help='disable JIT compilation')
    parser.add_argument('--x64', action='store_true',
            help='64-bit mode')
    parser.add_argument('--seed', type=int, 
            help="random seed")
    parser.add_argument('-S', '--samples', type=int, default=1000,
            help="samples per step of gradient descent")
    parser.add_argument('-T', '--steps', type=int, default=10000,
            help="number of gradient descent steps to perform")
    parser.add_argument('-W', '--width', type=int, default=1,
            help="width scaling")
    parser.add_argument('-D', '--depth', type=int, default=1,
            help="depth")
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('contour', type=str, help="contour filename")
    args = parser.parse_args()

    if args.debug_nan:
        jax.config.update("jax_debug_nans", True)
    if args.debug_inf:
        jax.config.update("jax_debug_infs", True)
    if args.no_jit:
        jax.config.update("jax_disable_jit", True)
    if args.x64:
        jax.config.update("jax_enable_x64", True)

    jax.config.update('jax_platform_name', 'cpu')

    # PRNG
    if args.seed is None:
        seed = time.time_ns()
    else:
        seed = args.seed
    init_key, mc_key = jax.random.split(jax.random.PRNGKey(seed))

    # Model, contour, and effective action
    with open(args.model, 'rb') as f:
        model = eval(f.read())
    dof = model.dof
    args._dof = int(dof)
    contour = make(args, key=init_key)

    def effective_action(x, c):
        z, ld = c(x)
        return model.action(z) - ld

    def neg_mean_real_action(c, x):
        S = jax.vmap(lambda y: effective_action(y, c))(x)
        return -jnp.mean(S.real)

    # Markov chain
    if True:
        mc = mcmc.ReplicaExchange(effective_action)
        chain = mc.init(jnp.zeros(model.dof), contour, 10, 100., key=mc_key)
    else:
        mc = mcmc.Metropolis(effective_action)
        chain = mc.init(jnp.zeros(model.dof), contour, key=mc_key)
    chain = mc.calibrate(chain)

    # Gradient descent
    opt = optax.adam(1e-3)
    opt_state = opt.init(eqx.filter(contour, eqx.is_array))

    @eqx.filter_jit
    def step(chain, contour, opt_state):
        skip = dof

        # Collect samples
        def collect(i, chain, x):
            chain = mc.step(chain, N=skip)
            x = x.at[i,:].set(chain.current())
            return chain, x
        x = jnp.zeros((args.samples,dof))
        chain_params, chain_static = eqx.partition(chain, eqx.is_array_like)
        def collect_filtered(i, dat):
            chain_params, x = dat
            chain = eqx.combine(chain_params, chain_static)
            chain, x = collect(i, chain, x)
            chain_params, _ = eqx.partition(chain, eqx.is_array_like)
            return chain_params, x
        chain_params, x = jax.lax.fori_loop(0, args.samples, collect_filtered, (chain_params, x))
        chain = eqx.combine(chain_params, chain_static)

        # Average phase
        actions = jax.vmap(lambda y: effective_action(y, contour))(x)

        # Gradient
        grad = eqx.filter_grad(neg_mean_real_action)(contour, x)
        updates, opt_state = opt.update(grad, opt_state)
        contour = eqx.apply_updates(contour, updates)

        chain = eqx.tree_at(lambda c: c.p, chain, contour)

        return chain, contour, opt_state, jnp.mean(jnp.exp(-1j*actions.imag))

    # Training loop
    start = time.time()
    try:
        for k in range(args.steps):
            chain, contour, opt_state, phase = step(chain, contour, opt_state)
            stats = mc.statistics(chain)
            print(f'{k:6}   {phase:.4f} {time.time()-start:8.2f}   | {stats["acceptance_rate"]:.2}')
            #print(f'{k:6}   {phase:.4f} {time.time()-start:8.2f}   | {stats}')
    except KeyboardInterrupt:
        pass

    save(args.contour, args, contour)

