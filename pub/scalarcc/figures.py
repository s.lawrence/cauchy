#!/usr/bin/env python

import sys

import numpy as np

import matplotlib
from matplotlib import colors
import matplotlib.pyplot as plt
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

figs = [int(s) for s in sys.argv[1:]]

if len(figs) == 0:
    figs = [1, 2, 3, 4, 5, 6]


def read_data(f, dtype=np.complex128):
    return np.array([[dtype(x) for x in l.split()] for l in f.readlines()])


if 1 in figs:
    print('Making figure 1')
    with open('data/figure1/result_exact.dat') as f:
        dat_exact = read_data(f)
    with open('data/figure1/result_nf.dat') as f:
        dat_nf = read_data(f)

    plt.figure(figsize=(5.5, 3.6))
    plt.plot(dat_exact[:, 0].real, dat_exact[:, 1].real,
             label='Real (Exact)', color='green')
    plt.plot(dat_exact[:, 0].real, dat_exact[:, 1].imag,
             label='Imag (Exact)', color='red')
    plt.errorbar(dat_nf[:, 0].real, dat_nf[:, 1].real,
                 yerr=dat_nf[:, 2].real, label='Real', color='green', fmt='.')
    plt.errorbar(dat_nf[:, 0].real, dat_nf[:, 1].imag,
                 yerr=dat_nf[:, 2].imag, label='Imag', color='red', fmt='.')
    # plt.errorbar()
    plt.ylim([-0.2, 0.7])
    plt.xlabel('$\mathrm{Im}\; \lambda$')
    plt.ylabel('$\\langle \\phi^2\\rangle$')
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig('figure1.png', dpi=300)


if 2 in figs:
    print('Making figure 2')
    with open('data/figure2/log_l0.dat') as f:
        dat_l0 = read_data(f)
    with open('data/figure2/log_l1.dat') as f:
        dat_l1 = read_data(f)

    step_l0 = np.array(range(dat_l0.shape[0]))
    step_l1 = np.array(range(dat_l1.shape[0]))

    lo, hi = 0, 500
    step_l0 = step_l0[lo:hi]
    step_l1 = step_l1[lo:hi]
    dat_l0 = dat_l0[lo:hi]
    dat_l1 = dat_l1[lo:hi]

    plt.figure(figsize=(5.5, 3.6))
    # The error bar computation is a cheat that only works because <sigma> is so close to 1.
    plt.errorbar(
        step_l0, 1/np.abs(dat_l0[:, 1]), yerr=np.abs(dat_l0[:, 2]), fmt='.', label='Linear')
    plt.errorbar(
        step_l1, 1/np.abs(dat_l1[:, 1]), yerr=np.abs(dat_l1[:, 2]), fmt='.', label='Single layer')
    plt.ylim([0.88, 1.])
    plt.xlabel('$T$')
    plt.ylabel('$\\langle \\sigma\\rangle$')
    plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig('figure2.png', dpi=300)


if 3 in figs:
    print('Making figure 3')

    with open('data/figure3/z_grid.dat') as f:
        dat = read_data(f)

    Z = dat[:, 2]
    Z = Z.reshape((70, 70))
    Z = Z.transpose()
    Z = Z[::-1, ::-1]
    hi = .7
    lo = -.7

    F = np.log(Z)

    #H = F.imag/(2*np.pi)+np.pi
    H = F.imag/(2*np.pi)+0.5
    S = 0*Z.imag + 1.0
    V = 0*Z.imag + 1.0
    Frmin = np.min(F.real)
    Frmax = np.max(F.real)
    #S = (F.real-Frmin)/(Frmax-Frmin)
    V = (F.real-Frmin)/(Frmax-Frmin)
    #V = np.abs(Z)/np.max(np.abs(Z))

    HSV = np.array([H, S, V]).transpose(1, 2, 0)
    RGB = colors.hsv_to_rgb(HSV)

    fig = plt.figure(figsize=(4.8, 3.0))
    ax_plt, ax_key = fig.subplots(1, 2, gridspec_kw={'width_ratios': [2, 1]})

    ax_plt.imshow(RGB, origin='lower', extent=[
                  lo, hi, lo, hi], interpolation='bilinear')
    ax_plt.set_xlabel('$\\mathrm{Re}\\;\\lambda$')
    ax_plt.set_ylabel('$\\mathrm{Im}\\;\\lambda$')

    bound = np.max(np.abs(Z))
    x, y = np.meshgrid(
        np.linspace(-bound, bound, 300),
        np.linspace(-bound, bound, 300))
    z = x+1j*y
    f = np.log(z)
    keyH = f.imag/(2*np.pi)+0.5
    keyS = z.imag*0 + 1.0
    keyV = (f.real-Frmin)/(Frmax-Frmin)

    keyHSV = np.array([keyH, keyS, keyV]).transpose(1, 2, 0)
    keyRGB = colors.hsv_to_rgb(keyHSV)

    ax_key.imshow(keyRGB, extent=[-bound, bound, -bound, bound])
    ax_key.set_xlabel('$\\mathrm{Re}\\;Z$')
    ax_key.set_ylabel('$\\mathrm{Im}\\;Z$')
    ax_key.set_xticks([-600, 0, 600])
    ax_key.set_yticks([-600, 0, 600])

    fig.tight_layout()
    fig.savefig('figure3.png', dpi=300)

    if False:
        from scipy.interpolate import interp2d

        with open('data/figure3/z.dat') as f:
            dat = read_data(f)

        # The set of (x,y) values at which we want Z.
        lo, hi = -.7, .7
        res = 120
        X, Y = np.meshgrid(np.linspace(lo, hi, res), np.linspace(lo, hi, res))

        # Requested R, Theta
        R = np.sqrt(X**2 + Y**2)
        Theta = np.arctan2(Y, X)

        # Interpolation functions
        r = dat[:, 0].real
        theta = dat[:, 1].real
        x = r*np.cos(theta)
        y = r*np.sin(theta)

        @np.vectorize
        def zfunc(R, Theta):
            dist = (R-r)**2 + (Theta-theta)**2
            a = 0.03
            fact = np.exp(-dist/a**2)
            num = np.sum(dat[:, 2]*fact)
            den = np.sum(fact)
            return num/den

        # Evaluate partition function
        Z = zfunc(R, Theta)

        plt.figure(figsize=(4.5, 3.6))
        plt.imshow(Z.imag, origin='lower', extent=[
                   lo, hi, lo, hi], interpolation='bilinear')
        plt.xlabel('$\\mathrm{Re}\\;\\lambda$')
        plt.ylabel('$\\mathrm{Im}\\;\\lambda$')
        plt.tight_layout()
        plt.savefig('figure3.png', dpi=300)


if 4 in figs:
    print('Making figure 4')
    with open('data/figure4/sign.dat') as f:
        dat = read_data(f)

    plt.figure(figsize=(5.5, 3.6))
    plt.errorbar(dat[:, 0].real, np.abs(dat[:, 1]),
                 yerr=np.abs(dat[:, 2]), fmt='o', label='$R^N$')
    plt.errorbar(dat[:, 0].real, np.abs(dat[:, 3]),
                 yerr=np.abs(dat[:, 4]), fmt='v', label='Trained')
    plt.plot(dat[:, 0].real, 0.915522*np.exp(-0.0891597*dat[:, 0]),'k')
    plt.plot(dat[:, 0].real, 0.947392*np.exp(-0.00439657*dat[:, 0]),'r')
    plt.yscale('log')
    plt.xlabel('$V$')
    plt.ylabel('$\\langle \\sigma\\rangle$')
    plt.xlim([4, 85])
    plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig('figure4.png', dpi=300)


if 5 in figs:
    print('Making figure 5')

    with open('data/fig56.dat') as f:
        dat = read_data(f)

    lamda = dat[:, 0]
    phi2 = dat[:, 5]
    phi2err = dat[:, 6]

    i04 = lamda.imag == 0.4
    i10 = lamda.imag == 1.0
    i20 = lamda.imag == 2.0

    plt.figure(figsize=(5.5, 3.6))
    plt.errorbar(lamda[i04].real, phi2[i04].real, yerr=phi2err[i04].real,
                 fmt='.', label='$\\mathrm{Im}\\; \\lambda = 0.4$')
    plt.errorbar(lamda[i10].real, phi2[i10].real, yerr=phi2err[i10].real,
                 fmt='.', label='$\\mathrm{Im}\\; \\lambda = 1.0$')
    plt.errorbar(lamda[i20].real, phi2[i20].real, yerr=phi2err[i20].real,
                 fmt='.', label='$\\mathrm{Im}\\; \\lambda = 2.0$')
    plt.legend(loc='best')
    plt.ylim([0, 0.30])
    plt.xlim([-0.1, 2.1])
    plt.xlabel('$\\mathrm{Re}\\;\\lambda$')
    plt.ylabel('$\\mathrm{Re}\\;\\langle\\phi^2\\rangle$')
    plt.tight_layout()
    plt.savefig('figure5-left.png', dpi=300)

    plt.figure(figsize=(5.5, 3.6))
    for lI, shape in [(0.45, 'o'), (1.05, 'v'), (2.15, 'd')]:
        lRs = [0.05 + 0.1*n for n in range(20)]
        #lRs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9]
        diffs = []
        for lR in lRs:
            lIp = lI+0.05
            lIm = lI-0.05
            lRp = lR+0.05
            lRm = lR-0.05

            lpp = lRp + 1j*lIp
            lpm = lRp + 1j*lIm
            lmp = lRm + 1j*lIp
            lmm = lRm + 1j*lIm

            fpp = phi2[np.isclose(lamda, lpp)].item()
            fpm = phi2[np.isclose(lamda, lpm)].item()
            fmp = phi2[np.isclose(lamda, lmp)].item()
            fmm = phi2[np.isclose(lamda, lmm)].item()

            dfdx = ((fpp+fpm)/2. - (fmp+fmm)/2.)/0.1
            dfdy = ((fpp+fmp)/2. - (fpm+fmm)/2.)/0.1
            partial = 0.5 * (dfdx + 1j*dfdy)

            diffs.append(np.abs(partial))
        plt.scatter(
            lRs, diffs, label=f'$\\mathrm{{Im}}\\;\\lambda = {lI}$', marker=shape)
    plt.xlim([-0.1, 2.1])
    plt.ylim([-0.001, 0.03])
    plt.xlabel('$\\mathrm{Re}\\;\\lambda$')
    plt.ylabel('$|\\bar\\Delta\\langle\\phi^2\\rangle|$')
    plt.tight_layout()
    plt.legend(loc='best')
    plt.savefig('figure5-right.png', dpi=300)

    if False:
        plt.figure(figsize=(5.5, 3.6))
        for i, l in enumerate(lamda):
            f = phi2[i]
            print(i, l)
        plt.xlabel('$\\mathrm{Re}\\;\\lambda$')
        plt.ylabel('$\\mathrm{Im}\\;\\lambda$')
        plt.tight_layout()
        plt.savefig('figure5-right.png', dpi=300)


if 6 in figs:
    print('Making figure 6')
    with open('data/fig56.dat') as f:
        dat = read_data(f)

    lamda = dat[:, 0]
    phase = dat[:, 1]
    phase /= np.abs(phase)

    lamda = np.append(lamda, np.linspace(0, 2, 21))
    phase = np.append(phase, [1 for _ in range(21)])

    plt.figure(figsize=(5.5, 3.6))
    plt.quiver(lamda.real, lamda.imag, phase.real, phase.imag, pivot='mid')
    plt.xlim([-0.1, 2.1])
    plt.ylim([-0.1, 3.1])
    # plt.legend(loc='best')
    plt.xlabel('$\\mathrm{Re}\\;\\lambda$')
    plt.ylabel('$\\mathrm{Im}\\;\\lambda$')
    plt.tight_layout()
    plt.savefig('figure6.png', dpi=300)
