#!/usr/bin/env python

import re

with open('data/figure5_6_update.txt') as f:
    txt = f.read()

paras = txt.split("\n\n")
for p in paras:
    lines = p.split("\n")
    if len(lines) > 2:
        lines = lines[:2]
    lamda = lines[0].replace('i','j')
    dat = [complex(x) for x in lines[1].split()]
    print(f'({lamda}) {dat[0]} {dat[1]} {dat[2]} {dat[3]} {dat[4]} {dat[5]}')


if False:
    with open('data/figure5_6.txt') as f:
        txt = f.read()

    paras = txt.split("\n\n")
    for p in paras:
        lines = p.split("\n")
        if not re.match('Lambda',lines[0]):
            continue
        lamda = lines[0].replace('Lambda=','').replace('i','j')
        rew = lines[1].replace('Reweighting: ','')
        phi2 = lines[2]
        print(f'({lamda}) {rew} {phi2}')

