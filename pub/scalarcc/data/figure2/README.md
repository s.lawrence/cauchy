Data for figure 2 made on 4/24/2022 with most up-to-date codes

layer 0:
./mkmodel.py data/scanlayer/model.pickle scalar '()' 10 0 0.5 0.1+1.0j
./nf.py data/scanlayer/model.pickle data/scanlayer/nf_l0.pickle -a -l0 -S15 -t1.0
./nf.py data/scanlayer/model.pickle data/scanlayer/nf_l0.pickle -l0 -S15 -t0.001 > data/scanlayer/log_l0.dat (about 20 minutes on Gunnison)


layer 1:
./nf.py data/scanlayer/model.pickle data/scanlayer/nf_l1.pickle -a -l1 -S15 -t1.0
./nf.py data/scanlayer/model.pickle data/scanlayer/nf_l1.pickle -l1 -S15 -t0.001 > data/scanlayer/log_l1.dat (about 50 minutes on Gunnison)

