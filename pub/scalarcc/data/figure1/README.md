Datas are created on 4/24/2022 with the most up-to-date codes.
Datas from normalizing flow was created using "imlambda.sh" and then the expectation values of phi^2 were copied to result_nf.dat from result_0.dat to result_10.dat. The format of result_nf.dat is
imaginary part of lambda, expectation value of phi^s, error

Exact results in result_exact.dat was gained using "exact.sh". The format of result_exact.dat is
imaginary part of lambda, expectation value of phi^2 

