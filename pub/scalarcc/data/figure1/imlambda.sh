./mkmodel.py data/scanImlambda/model_10_0_05_0.pickle scalar '()' 10 0 0.5 -- 0.10 
./nf.py data/scanImlambda/model_10_0_05_0.pickle data/scanImlambda/nf.pickle -a -l2 -S15 -t0.5
./nf.py data/scanImlambda/model_10_0_05_0.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_0.pickle
./gaussian.py data/scanImlambda/model_10_0_05_0.pickle data/scanImlambda/nf_10_0_05_0.pickle -B100 > data/scanImlambda/sample_0.dat
./bootstrap.py < data/scanImlambda/sample_0.dat > data/scanImlambda/result_0.dat
echo "************ 1 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_1.pickle scalar '()' 10 0 0.5 -- 0.10+0.10j
./nf.py data/scanImlambda/model_10_0_05_1.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_1.pickle
./gaussian.py data/scanImlambda/model_10_0_05_1.pickle data/scanImlambda/nf_10_0_05_1.pickle -B100 > data/scanImlambda/sample_1.dat
./bootstrap.py < data/scanImlambda/sample_1.dat > data/scanImlambda/result_1.dat
echo "************ 2 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_2.pickle scalar '()' 10 0 0.5 -- 0.10+0.20j
./nf.py data/scanImlambda/model_10_0_05_2.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_2.pickle
./gaussian.py data/scanImlambda/model_10_0_05_2.pickle data/scanImlambda/nf_10_0_05_2.pickle -B100 > data/scanImlambda/sample_2.dat
./bootstrap.py < data/scanImlambda/sample_2.dat > data/scanImlambda/result_2.dat
echo "************ 3 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_3.pickle scalar '()' 10 0 0.5 -- 0.10+0.30j
./nf.py data/scanImlambda/model_10_0_05_3.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_3.pickle
./gaussian.py data/scanImlambda/model_10_0_05_3.pickle data/scanImlambda/nf_10_0_05_3.pickle -B100 > data/scanImlambda/sample_3.dat
./bootstrap.py < data/scanImlambda/sample_3.dat > data/scanImlambda/result_3.dat
echo "************ 4 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_4.pickle scalar '()' 10 0 0.5 -- 0.10+0.40j
./nf.py data/scanImlambda/model_10_0_05_4.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_4.pickle
./gaussian.py data/scanImlambda/model_10_0_05_4.pickle data/scanImlambda/nf_10_0_05_4.pickle -B100 > data/scanImlambda/sample_4.dat
./bootstrap.py < data/scanImlambda/sample_4.dat > data/scanImlambda/result_4.dat
echo "************ 5 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_5.pickle scalar '()' 10 0 0.5 -- 0.10+0.50j
./nf.py data/scanImlambda/model_10_0_05_5.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_5.pickle
./gaussian.py data/scanImlambda/model_10_0_05_5.pickle data/scanImlambda/nf_10_0_05_5.pickle -B100 > data/scanImlambda/sample_5.dat
./bootstrap.py < data/scanImlambda/sample_5.dat > data/scanImlambda/result_5.dat
echo "************ 6 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_6.pickle scalar '()' 10 0 0.5 -- 0.10+0.60j
./nf.py data/scanImlambda/model_10_0_05_6.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_6.pickle
./gaussian.py data/scanImlambda/model_10_0_05_6.pickle data/scanImlambda/nf_10_0_05_6.pickle -B100 > data/scanImlambda/sample_6.dat
./bootstrap.py < data/scanImlambda/sample_6.dat > data/scanImlambda/result_6.dat
echo "************ 7 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_7.pickle scalar '()' 10 0 0.5 -- 0.10+0.70j
./nf.py data/scanImlambda/model_10_0_05_7.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_7.pickle
./gaussian.py data/scanImlambda/model_10_0_05_7.pickle data/scanImlambda/nf_10_0_05_7.pickle -B100 > data/scanImlambda/sample_7.dat
./bootstrap.py < data/scanImlambda/sample_7.dat > data/scanImlambda/result_7.dat
echo "************ 8 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_8.pickle scalar '()' 10 0 0.5 -- 0.10+0.80j
./nf.py data/scanImlambda/model_10_0_05_8.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_8.pickle
./gaussian.py data/scanImlambda/model_10_0_05_8.pickle data/scanImlambda/nf_10_0_05_8.pickle -B100 > data/scanImlambda/sample_8.dat
./bootstrap.py < data/scanImlambda/sample_8.dat > data/scanImlambda/result_8.dat
echo "************ 9 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_9.pickle scalar '()' 10 0 0.5 -- 0.10+0.90j
./nf.py data/scanImlambda/model_10_0_05_9.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_9.pickle
./gaussian.py data/scanImlambda/model_10_0_05_9.pickle data/scanImlambda/nf_10_0_05_9.pickle -B100 > data/scanImlambda/sample_9.dat
./bootstrap.py < data/scanImlambda/sample_9.dat > data/scanImlambda/result_9.dat
echo "************ 10 ************"
./mkmodel.py data/scanImlambda/model_10_0_05_10.pickle scalar '()' 10 0 0.5 -- 0.10+1.00j
./nf.py data/scanImlambda/model_10_0_05_10.pickle data/scanImlambda/nf.pickle -l2 -S15 -t0.010
cp data/scanImlambda/nf.pickle  data/scanImlambda/nf_10_0_05_10.pickle
./gaussian.py data/scanImlambda/model_10_0_05_10.pickle data/scanImlambda/nf_10_0_05_10.pickle -B100 > data/scanImlambda/sample_10.dat
./bootstrap.py < data/scanImlambda/sample_10.dat > data/scanImlambda/result_10.dat
