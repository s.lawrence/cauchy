The file 'z.dat' contains the partition function of the complex coupling model while changing the coupling. The other parameters are, Nb = 10, m2 = 0.5. The normalizing flow has layers of -l1. For each coupling, the normalizing flow was trained down to -t0.08. The format of the file is
radius of coupling, phase of coupling, partition function, error
for r=0.1, 0.2, ..., 1.0. The phase was taken evenly with the step of 2pi/(100*r). The sample data was generated with
./gaussian.py model nf -S10000 -B100
and passed into ./bootstrap.py to create z.dat.

The file 'z_grid.dat' was created on 5/4/2022 with the most-up-to-date code. The file contains the partition function on the grid in -0.7 < Re(lambda), Im(lambda) < +0.7 wish 70 vertecies in each dimension. The format of the file is
real part of lambda, imaginary part of lambda, partition function, error
The file was created with 'complscan_add.sh' which uses the normalizing flows created above and then 
./gaussian.py model nf -S1000 -B100
and bootstrap.py.
Additionally, for 16 couplings Re(lambda), Im(lambda) = (0.03,0.01,-0.01,0.03), corresponding normalizing flows of the nearest angle and radius or 0.1 were used. Additionally since the normalizing flows are bad at r=-0.1, angle = \pm pi, they were retrained.
