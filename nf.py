#!/usr/bin/env python

from functools import partial
import os
import pickle
import sys
import time
from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
from jax.scipy import special
import numpy as np
import optax

from models import scalar,trivial
from util import *

"""
TODO

Update observe() in scalar model
Don't work with reweighting factors directly, but use logsumexp as appropriate.
Better contours
Make logit NAN(inf?)-robust
Look again at the MIT papers to find better parameterizations

"""

class QuarticFlow(eqx.Module):
    """ The exact normalizing flow for a quartic action. """
    loglamda: jnp.array

    def __init__(self, lamda):
        self.loglamda = jnp.log(lamda)

    def __call__(self, x):
        lamda = jnp.exp(self.loglamda)
        scale = lamda**(-1/4)
        z = normalize_quartic(x) * scale.reshape(x.shape)
        d = jax.vmap(jax.grad(normalize_quartic))(x) * scale.reshape(x.shape)
        return z, jnp.sum(jnp.log(d+0j))

class NaiveContour(eqx.Module):
    mlp: nn.MLP
    final: nn.Linear

    def __init__(self, key, N, depth):
        W = 4*N

        key_mlp, key_f = jax.random.split(key, 2)
        self.mlp = nn.MLP(in_size=N, out_size=2*N, width_size=W, depth=depth, key=key_mlp)
        final = nn.Linear(2*N, 2*N, key=key_f, use_bias=True)
        self.final = eqx.tree_at(lambda l: l.weight, final, jnp.zeros((2*N,2*N)))

    def contour(self, x):
        y = self.mlp(x)
        y = self.final(jax.nn.tanh(y))
        y = y[::2] + 1j*y[1::2]
        return x+y

    def __call__(self, x):
        z = self.contour(x)
        jac = jax.jacfwd(self.contour)(x)
        phase, logdet = jnp.linalg.slogdet(jac)
        return z, logdet + jnp.log(phase+0j)

class LULinear(eqx.Module):
    lu: jnp.array

    def __init__(self, key, N):
        scale = 1/jnp.sqrt(N)
        lu = jnp.eye(N)
        self.lu = lu+jax.random.uniform(key, (N,N), minval=scale, maxval=scale)
        self.lu = lu

    def __call__(self, x):
        x = lu_mul(self.lu, x)
        ld = jnp.sum(jnp.log(jnp.diagonal(self.lu)))
        return x, ld

class LUFlow(eqx.Module):
    layers: list
    activation: Callable

    def __init__(self, key, N, depth, activation=leaky_tanh):
        keys = jax.random.split(key, depth+1)
        self.layers = [LULinear(k, N) for k in keys]
        self.activation = activation

    def __call__(self, x):
        ld = 0.
        for l in self.layers[:-1]:
            x, ld_ = l(x)
            ld += ld_
            x, g = jax.vmap(jax.value_and_grad(self.activation))(x)
            ld += jnp.sum(jnp.log(g))
        x, ld_ = self.layers[-1](x)
        ld += ld_
        return x, ld

class Sigmoidal(eqx.Module):
    """ A single sigmoidal layer. """
    w: jnp.array
    a: jnp.array
    b: jnp.array

    def __init__(self, key, N, modes=2):
        keyw, keya, keyb = jax.random.split(key, 3)
        self.w = jax.random.uniform(keyw, (modes,))
        self.a = jax.random.uniform(keya, (modes,N)) * 1e-1 + 0.54
        self.b = jax.random.uniform(keya, (modes,N)) * 1e-1

    def __call__(self, x):
        # Normalize weights with softmax
        w = jax.nn.softmax(self.w)
        a = jax.nn.softplus(self.a)
        y = jnp.einsum('ki,i->ki', a, x) + self.b
        z = jnp.einsum('k,ki->i', w, jax.nn.sigmoid(y))
        dsigmoid = jax.vmap(jax.vmap(jax.grad(jax.nn.sigmoid)))
        dlogit = jax.vmap(jax.grad(special.logit))
        d = jnp.einsum('i,k,ki,ki->i', dlogit(z), w, dsigmoid(y), a)
        return special.logit(z), jnp.sum(jnp.log(d))

class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: nn.MLP
    trans: nn.MLP

    def __init__(self, key, N, mask=None):
        if mask is None:
            key, key_mask = jax.random.split(key)
            self.mask = jax.random.randint(key, (N,), 0, 2)
        else:
            self.mask = mask
        key_scale, key_trans = jax.random.split(key)
        scale = nn.MLP(
                in_size=N,
                out_size=N,
                width_size=N,
                depth=1,
                key=key_scale,
                activation=jnp.tanh)
        trans = nn.MLP(in_size=N,
                       out_size=N,
                       width_size=N,
                       depth=0,
                       key=key_scale)
        self.scale = zero_mlp(scale)
        self.trans = zero_mlp(trans)

    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask)*s)

class CheckeredAffines(eqx.Module):
    even: AffineCoupling
    odd: AffineCoupling

    def __init__(self, key, N):
        # TODO this checkerboard pattern isn't correct in more than one dimension
        key_even, key_odd = jax.random.split(key)
        mask_even = jnp.arange(N)%2
        mask_odd = 1 - (jnp.arange(N)%2)
        self.even = AffineCoupling(key_even, N, mask=mask_even)
        self.odd = AffineCoupling(key_odd, N, mask=mask_odd)

    def __call__(self, x):
        x, ld_e = self.even(x)
        x, ld_o = self.odd(x)
        return x, ld_e + ld_o

class RealNVP(eqx.Module):
    layers: list

    def __init__(self, key, N, depth, checkered = True):
        keys = jax.random.split(key, depth)
        if checkered:
            self.layers = [CheckeredAffines(k, N) for k in keys]
        else:
            self.layers = [AffineCoupling(k, N) for k in keys]

    def __call__(self, x):
        ld = 0.
        for l in self.layers:
            x, ld_ = l(x)
            ld += ld_
        return x, ld

class CompositeFlow(eqx.Module):
    parts: list

    def __init__(self, *parts):
        self.parts = parts

    def __call__(self, x):
        ld = 0
        for p in self.parts:
            x, ld_ = p(x)
            ld += ld_
        return x, ld

class Mixture(eqx.Module):
    def __init__(self, *flows):
        # TODO
        pass

    def __call__(self, x):
        pass

class LUContour(eqx.Module):
    lu_r: jnp.array
    lu_i: jnp.array

    def __init__(self, key, N):
        self.lu_r = jnp.eye(N)
        self.lu_i = jnp.zeros((N,N))

    def __call__(self, x):
        lu = self.lu_r + 1j*self.lu_i
        x = lu_mul(lu, x)
        ld = jnp.sum(jnp.log(jnp.diagonal(lu)))
        return x, ld

class TriangularContour(eqx.Module):
    layers: list

    def __init__(self, key, N, width, depth):
        self.layers = []
        # TODO use width as a scaling factor (but keep triangle-ness)

    def __call__(self, x):
        return x, 0. # TODO

class HolomorphicAffineCoupling(eqx.Module):
    def __init__(self):
        pass

    def __call__(self, x):
        pass

class HolomorphicNVP(eqx.Module):
    def __init__(self):
        pass

    def __call__(self, x):
        pass

def make(key, layers, model):
    dof = model.dof
    coefs = jnp.abs((model.dt_site)*model.lamda)

    fkey, ckey = jax.random.split(key)
    initial = QuarticFlow(coefs)

    if False:
        nvp = RealNVP(key=fkey, depth=layers, N=dof, checkered=False)
        flow = CompositeFlow(initial, nvp)
    else:
        fkeys = jax.random.split(fkey, layers)
        ls = []
        for l in range(layers):
            ls.append(Sigmoidal(key=fkeys[l], N=dof))
            ls.append(LULinear(key=fkeys[l], N=dof))
        flow = CompositeFlow(initial, *ls)

    contour = LUContour(key=ckey, N=dof)
    return flow, contour

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Train normalizing flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('--debug-nan', action='store_true',
            help='nan debugging mode')
    parser.add_argument('--debug-inf', action='store_true',
            help='inf debugging mode')
    parser.add_argument('--no-jit', action='store_true',
            help='disable JIT compilation')
    parser.add_argument('-R', '--real', action='store_true',
            help='do not train contour')
    parser.add_argument('-i', '--init', action='store_true',
            help='re-initialize flow')
    parser.add_argument('-t', '--threshold', type=float, default=0.1,
            help='adiabatic threshold')
    parser.add_argument('-l', '--layers', type=int, default=1,
            help='number of (hidden) layers')
    parser.add_argument('-S', '--samples', default=(1<<9), type=int,
            help='number of samples')
    parser.add_argument('-P', '--no-pre', action='store_true',
            help='do not perform adiabatic pre-training')
    parser.add_argument('--x64', action='store_true',
            help='64-bit mode')
    parser.add_argument('--seed', type=int, 
            help="random seed")
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('nf', type=str, help="flow filename")
    args = parser.parse_args()

    if args.debug_nan:
        jax.config.update("jax_debug_nans", True)
    if args.debug_inf:
        jax.config.update("jax_debug_infs", True)
    if args.no_jit:
        jax.config.update("jax_disable_jit", True)
    if args.x64:
        jax.config.update("jax_enable_x64", True)

    with open(args.model, 'rb') as f:
        model = eval(f.read())

    # PRNG
    if args.seed is None:
        seed = time.time_ns()
    else:
        seed = args.seed
    sampleKey, initKey = jax.random.split(jax.random.PRNGKey(seed))

    #HBARS = jnp.array([1., 2., 3.])**2
    HBARS = jnp.array([1.])

    # Initialize flow and contour
    flow, contour = make(initKey, args.layers, model)
    init = args.init or not os.path.isfile(args.nf)
    if not init:
        flow, contour = eqx.tree_deserialise_leaves(args.nf, (flow, contour))

    def sample_hbar(xk, hbar):
        xs = jax.random.normal(xk, (args.samples, model.dof)) * jnp.sqrt(hbar)
        act_normal = jnp.sum(xs**2 / 2., axis=-1)
        act_sampled = jnp.sum(xs**2 / (2. * hbar), axis=-1)
        return xs, act_normal, act_sampled

    def sample(xk):
        xs_, act_normal_, act_sampled_ = [], [], []
        for hbar in HBARS:
            k, xk = jax.random.split(xk)
            xs, act_normal, act_sampled = sample_hbar(k, hbar)
            xs_.append(xs)
            act_normal_.append(act_normal)
            act_sampled_.append(act_sampled)
        return jnp.concatenate(xs_), jnp.concatenate(act_normal_), jnp.concatenate(act_sampled_)

    @partial(jnp.vectorize, signature='(i),()->()', excluded={0,1,2})
    def reweight(flow, contour, t, x, act_samp):
        y, fld = flow(x)
        z, cld = contour(y)
        ld = fld + cld
        act = model.action(z, t=t)
        return jnp.exp(act_samp - act + ld)

    @partial(jnp.vectorize, signature='(i),()->()', excluded={0,1,2})
    def logreweight(flow, contour, t, x, act_samp):
        y, fld = flow(x)
        z, cld = contour(y)
        ld = fld + cld
        act = model.action(z, t=t)
        return act_samp - act + ld

    @partial(jnp.vectorize, signature='(i),()->()', excluded={0,1,2})
    def flow_loss_at(flow, contour, t, x, act_samp):
        y, fld = flow(x)
        z, cld = contour(y)
        ld = fld + cld
        act = model.action(z, t=t)
        return act_samp - act.real + ld.real

    def flow_loss(flow, contour, xk, t=1.):
        xs, act_norm, act_samp = sample(xk)
        floss = flow_loss_at(flow, contour, t, xs, act_norm)
        rew = jnp.abs(reweight(flow, contour, t, xs, act_samp))
        rkl = jnp.mean(rew*floss) / jnp.mean(rew)
        kl = jnp.mean(-floss)
        merit = jnp.mean(rew*floss*floss) / jnp.mean(rew) - rkl**2
        return rkl+kl, merit

    fgrad_fn = eqx.filter_jit(eqx.filter_value_and_grad(flow_loss, has_aux=True))

    @partial(jnp.vectorize, signature='(i)->()', excluded={0,1})
    def real_effective_action(contour, flow, x):
        y, fld = flow(x)
        z, cld = contour(y)
        ld = fld + cld
        act = model.action(z)
        return act.real - ld.real

    def mean_neg_real_effective_action(contour, flow, xk):
        xs, act_samp, _ = sample_hbar(xk, 1.)
        Seff_r = real_effective_action(contour, flow, xs)
        rew = jax.lax.stop_gradient(jnp.abs(reweight(flow, contour, 1., xs, act_samp)))
        return jnp.mean(-rew*Seff_r) / jnp.mean(rew)

    @eqx.filter_jit
    def cgrad_fn(contour, flow, xk):
        return eqx.filter_grad(mean_neg_real_effective_action)(contour, flow, xk)

    @partial(jnp.vectorize, signature='(i)->()', excluded={0,1})
    def phase(flow, contour, x):
        y, fld = flow(x)
        z, cld = contour(y)
        ld = fld + cld
        act = model.action(z)
        return jnp.exp(1j * (act-ld).imag)

    @eqx.filter_jit
    def average_phase(flow, contour, xk):
        xs, act_samp, _ = sample_hbar(xk, 1.)
        rew = jnp.abs(reweight(flow, contour, 1., xs, act_samp))
        ph = phase(flow, contour, xs)
        return jnp.mean(ph*rew) / jnp.mean(rew)

    # Optimization
    fopt = optax.adam(1e-3)
    fopt_state = fopt.init(eqx.filter(flow, eqx.is_array))
    copt = optax.adam(3e-4)
    copt_state = copt.init(eqx.filter(contour, eqx.is_array))

    start = time.time()

    try:
        # Adiabatic pre-training
        if init:
            ts = np.linspace(0,1,101)
            if args.no_pre:
                ts = [1.]
            for t in ts:
                # Update normalizing flow
                fmerit = 1e10
                step = 0
                #while fmerit > (.1+.9*t)*args.threshold: # TODO make this a bit more systematic
                while fmerit > args.threshold:
                    sampleKey, xk = jax.random.split(sampleKey)
                    (floss, fmerit), fgrad = fgrad_fn(flow, contour, xk, t)
                    fupdates, fopt_state = eqx.filter_jit(fopt.update)(fgrad, fopt_state)
                    flow = eqx.filter_jit(eqx.apply_updates)(flow, fupdates)
                    step += 1
                    cumulative = time.time() - start
                    print(f' PRE {t:6.3} | {floss:9.4f} {fmerit:9.4f} |  ({step})       {cumulative:8.1f}')

        if not args.real:
            for c in range(1000):
                # Update normalizing flow
                fmerit = 1e10
                while fmerit > args.threshold:
                    sampleKey, xk = jax.random.split(sampleKey)
                    (floss, fmerit), fgrad = fgrad_fn(flow, contour, xk)
                    fupdates, fopt_state = eqx.filter_jit(fopt.update)(fgrad, fopt_state)
                    flow = eqx.filter_jit(eqx.apply_updates)(flow, fupdates)
                    cumulative = time.time() - start
                    print(f'{c:5} | {floss:9.4f} {fmerit:9.4f} |   {average_phase(flow, contour, xk)}       {cumulative:8.1f}')

                # Update contour
                sampleKey, xk = jax.random.split(sampleKey)
                cgrad = cgrad_fn(contour, flow, xk)
                cupdates, copt_state = eqx.filter_jit(copt.update)(cgrad, copt_state)
                contour = eqx.filter_jit(eqx.apply_updates)(contour, cupdates)
                print(f'   UPDATE CONTOUR    {average_phase(flow, contour, xk)}')
    except KeyboardInterrupt:
        pass
    eqx.tree_serialise_leaves(args.nf, (flow,contour))

    if False: # For investigating norms
        def norm(x):
            return jnp.sqrt(jnp.sum(x**2))
        norms = jax.tree_map(norm,eqx.filter(flow, eqx.is_array))
        eqx.tree_pprint(norms, short_arrays=False)
        eqx.tree_pprint(flow, short_arrays=False)

