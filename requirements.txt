absl-py==1.1.0
attrs==21.4.0
chex==0.1.3
cycler==0.11.0
dm-tree==0.1.7
flatbuffers==2.0
flax==0.5.0
fonttools==4.33.3
iniconfig==1.1.1
jax==0.3.14
jaxlib==0.3.10
kiwisolver==1.4.2
matplotlib==3.5.2
msgpack==1.0.4
numpy==1.22.4
opt-einsum==3.3.0
optax==0.1.2
packaging==21.3
Pillow==9.1.1
pluggy==1.0.0
py==1.11.0
pyparsing==3.0.9
pytest==7.1.2
python-dateutil==2.8.2
scipy==1.8.1
six==1.16.0
tomli==2.0.1
toolz==0.11.2
typing_extensions==4.2.0
