#!/usr/bin/env python

import equinox as eqx
import jax
import jax.numpy as jnp
import optax

from econtour import *

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Sample from contour",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('--debug-nan', action='store_true',
            help='nan debugging mode')
    parser.add_argument('--debug-inf', action='store_true',
            help='inf debugging mode')
    parser.add_argument('--no-jit', action='store_true',
            help='disable JIT compilation')
    parser.add_argument('--x64', action='store_true',
            help='64-bit mode')
    parser.add_argument('--seed', type=int, 
            help="random seed")
    parser.add_argument('--replica-exchange', action='store_true',
            help="use replica exchange")
    parser.add_argument('model', type=str, help="model filename")
    parser.add_argument('contour', type=str, help="contour filename")
    args = parser.parse_args()

    if args.debug_nan:
        jax.config.update("jax_debug_nans", True)
    if args.debug_inf:
        jax.config.update("jax_debug_infs", True)
    if args.no_jit:
        jax.config.update("jax_disable_jit", True)
    if args.x64:
        jax.config.update("jax_enable_x64", True)

    jax.config.update('jax_platform_name', 'cpu')

    # PRNG
    if args.seed is None:
        seed = time.time_ns()
    else:
        seed = args.seed
    mc_key = jax.random.PRNGKey(seed)

    with open(args.model, 'rb') as f:
        model = eval(f.read())
    contour = load(args.contour)

    def effective_action(x, c):
        z, ld = c(x)
        return model.action(z) - ld

    # Markov chain
    if args.replica_exchange:
        assert False # TODO
    mc = mcmc.Metropolis(effective_action)
    chain = mc.init(jnp.zeros(model.dof), contour, key=mc_key)
    chain = mc.calibrate(chain)

    @jax.jit
    def advance(chain):
        chain = mc.step(chain, N=model.dof)
        x = chain.current()
        action = effective_action(x, contour)
        obs = model.observe(contour(x)[0])
        return chain, action, obs

    try:
        while True:
            chain, action, obs = advance(chain)
            phase = jnp.exp(-1j*action.imag)
            print(f'{phase} {action}')
    except KeyboardInterrupt:
        pass

