#!/usr/bin/env python

import argparse
import sys

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def load(lines):
    for l in lines:
        if l.strip()[0] == '#':
            continue
        yield [complex(x) for x in l.split()]

parser = argparse.ArgumentParser(
        description="Make observations and plots",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        fromfile_prefix_chars='@'
        )
args = parser.parse_args()

dat = np.array(list(load(sys.stdin.readlines())))
cor = dat[1:-1]
beta = len(cor)
ts = np.arange(beta)

def fitf(t, a, m):
    return a*np.cosh(m * (beta/2 - t))

fit, cov = curve_fit(fitf, ts, cor[:,0])
print(f'Mass: {fit[1]}')
plt.errorbar(ts, cor[:,0], yerr = cor[:,1])
plt.plot(ts, fitf(ts, *fit))
plt.yscale('log')
plt.show()

