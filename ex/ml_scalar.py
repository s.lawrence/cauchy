#!/usr/bin/env python

# machine learning manifolds for scalar field theory in 0+1
# . ~/env/bin/activate

import numpy as np
import sys
import cmath
import math
import jax
import jax.numpy as jnp
import objax
import sk

nbeta = int(sys.argv[1])
nt = int(sys.argv[2])
m = float(sys.argv[3])
g = float(sys.argv[4])
NT = nbeta + 2*nt
Nadj = 1000         # number of sampling in each step of adjustment stepsize for the acceptance rate of 0.3~0.4
Nthm = 1000       # number of steps thermalization
Nskip = 20        # number of steps to skip in MCMC
Nsap = 1000       # number of MCMC samples per training
tr = 5e-3        # training stepsize

# Create model phi^4 scalar
model = sk.SK1D(nbeta, nt, m, g)

# Map from R to C
class Map(objax.Module):
    def __init__(self, V):
        # self.linear_real = objax.nn.Linear(V, V)
        self.linear_imag = objax.nn.Linear(V, V, True, jnp.zeros)
        # self.linear_imag = objax.nn.Linear(V, V)

    def __call__(self, x):
        x = x + 1j*self.linear_imag(x)
        return x

map = Map(model.V)

@objax.Jit
@objax.Function.with_vars(map.vars())
def Sef(x):
    j = jax.jacfwd(map)(x)
    d = jnp.linalg.det(j)
    xt = map(x)
    Seff = model.action(xt) - jnp.log(d)
    return Seff

# Real part of effective action S - Log Det J
def ReS(x):
    res = Sef(x).real    
    return res

gv = objax.Jit(objax.Grad(ReS, map.vars()))     #Can do just Grad

# MCMC one step
def step(x, d):
    xp = x + np.random.normal(0,d,len(x))
    sdif = ReS(xp) - ReS(x)
    r = jnp.exp(-sdif)
    if(r < 1):
        p = np.random.uniform(0,1)
        if(r < p):
            return x, 0
    return xp, 1

# Adjust delta such that acceptance rate fits 0.3~0.4
def dt_adjust(x, d, Nadj):
    a = 0
    x0 = x
    while(a < 0.3 or a > 0.4):
        if(a < 0.3):
            d = d*0.99
        elif(a > 0.4):
            d = d*1.01

        ac = 0  
        x = therm(x0, d, Nthm)   
        # Estimate current acceptance rate
        for m1 in range(Nadj):
            x, r = step(x,d)
            ac = ac+float(r)/float(Nadj)
            # if(np.mod(m1,100)==0):
            #     print(ac*float(Nadj)/(m1+1))
        a = ac
        print(d, a)
        if(d > 10):
            a = 0.35

    return d

# Thermalization loop
def therm(x, d, Nthm):
    for n in range(Nthm):
        x, r = step(x,d)
    return x

# MCMC sampling for a given Seff. Compute the gradients of e^(-Seff) with
# respect to parameters in network for each sample configs and return them.
# Right now, the fact that there are two matrix in parameters is used in
# coding.
def measure(x, d, Nsap, Nskip):
    x = therm(x, d, Nthm)
    sign = []
    gstore0 = []
    gstore1 = []
    for n in range(Nskip * Nsap):
        x, r = step(x,d)
        if(np.mod(n+1,Nskip)==0):
            imS = Sef(x).imag
            sign.append(np.exp(-1j*imS))
            g1 = gv(x)
            gstore0.append(g1[0])
            gstore1.append(g1[1])

    print('The last state and its ReS: ', x, ReS(x))

    return np.array(sign), np.array(gstore0), np.array(gstore1)

def stat(x, Sb, Nb):
    dimb = int(np.size(x,0)/float(Sb))
    xb = x[0:dimb]
    for m in range(dimb):
        xb[m] = np.mean(x[m*Sb:(m+1)*Sb], axis=0)

    bave = x[0:Nb]
    for m1 in range(Nb):
        i = np.random.randint(dimb, size=(dimb))
        xnew = xb[i]
        bave[m1] = np.mean(xnew, axis=0).real

    ave = np.mean(bave, axis=0)
    std = np.std(bave, axis=0)

    return ave, std


# Training
# opt = objax.Jit(objax.optimizer.Adam(map.vars()))
opt = objax.Jit(objax.optimizer.SGD(map.vars()))
x0 = np.random.normal(0,1.0,NT)
x = x0
dt = 1.0
ave = 0.
std = 0.
for n in range(1000):
    sign = []
    gstore0 = []
    gstore1 = []
    g = []
    print('-------------- Training number', n+1, '--------------')
    print('* Adjust dt *')
    dt = dt_adjust(x0, dt, Nadj)
    if (dt > 9):
        print('In the wrong homology class, quit')
        break
    print('* Compute gradients and average sign *')
    sign, gstore0, gstore1 = measure(x,dt,Nsap,Nskip)
    print('* Average sign *')
    ave, std = stat(sign, 20, 200)
    print(ave,'+/-',std)
    ave0, std0 = stat(np.array(gstore0), 20, 200)
    ave1, std1 = stat(np.array(gstore1), 20, 200)
    ave0 = -ave0
    ave1 = -ave1
    g.append(jnp.array(ave0/np.amax(np.abs(ave0))))
    g.append(jnp.array(ave1/np.amax(np.abs(ave1))))

    opt(tr, g)
    if(np.mod(n,5)==0):
        print('-- Parameters of neural network --') 
        for k, v in map.vars().items():
            print(f'{k}')
            print(f'{v.value}')

