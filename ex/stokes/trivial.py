#!/usr/bin/env python

""" Trivial removal of the (cos theta + epsilon) sign problem via subtractions.

The main purpose of this script is testing methods of computing observables.
"""

import numpy as np

eps = 0.01
def f(theta):
    return np.cos(theta) + eps

theta = np.linspace(-np.pi,np.pi,10000)

Z = np.trapz(f(theta),theta)
ZQ = np.trapz(np.abs(f(theta)),theta)

print('EXACT')
print(f'Sign: {Z/ZQ}')
print(f'Expectation: {np.trapz(np.cos(theta)*f(theta),theta)/Z}')
print()
print('MONTE CARLO')
theta = np.random.uniform(size=(1000,))*2*np.pi
print('No subtraction:', np.sum(np.cos(theta)*f(theta))/np.sum(f(theta)))

sub = np.cos(theta)
boltz = f(theta)
obs = np.cos(theta)
print('Subtraction 1:', np.sum((obs*boltz-sub)/(boltz-sub)*(boltz-sub))/np.sum(boltz-sub))

sub = np.cos(theta)
boltz = f(theta)
obs = np.cos(theta)
print('Subtraction 2:', np.sum((obs*boltz)/(boltz-sub)*(boltz-sub))/np.sum(boltz-sub))

sub = 0.9*np.cos(theta)
boltz = f(theta)
obs = np.cos(theta)
print('Subtraction 1:', np.sum((obs*boltz-sub)/(boltz-sub)*(boltz-sub))/np.sum(boltz-sub))

sub = 0.9*np.cos(theta)
boltz = f(theta)
obs = np.cos(theta)
print('Subtraction 2:', np.sum((obs*boltz)/(boltz-sub)*(boltz-sub))/np.sum(boltz-sub))

#print('Subtraction 1: ', np.sum(np.cos(theta)*f(theta))/np.sum(f(theta)))
#print('Subtraction 2: ', np.sum(np.cos(theta)*f(theta))/np.sum(f(theta)-np.cos(theta)))

