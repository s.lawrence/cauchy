from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp

class MLP(eqx.Module):
    activation: Callable = staticmethod(jax.nn.selu)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

