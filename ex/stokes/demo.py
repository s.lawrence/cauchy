#!/usr/bin/env python

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np
import optax

import mc.metropolis
import net

jax.config.update('jax_platform_name', 'cpu')

V = 2

def f(theta):
    eps = 0.1
    return jnp.prod(jnp.cos(theta) + eps)

def S(theta):
    return -jnp.log(f(theta)+0j)

class ExactForm(eqx.Module):
    """ An exact, top-rank differential form.
    """
    D: int
    mlp: net.MLP

    def __init__(self, key, D):
        self.D = D
        self.mlp = net.MLP(key, [2*D,2*D,2*D,D])
        
    def _v(self, x):
        return self.mlp(jnp.concatenate([jnp.cos(x), jnp.sin(x)]))

    def __call__(self, x):
        j = jax.jacfwd(self._v)(x)
        dv = jnp.sum(jnp.diagonal(j))
        return dv

chainKey, formKey = jax.random.split(jax.random.PRNGKey(0))

form = ExactForm(formKey, V)

@eqx.filter_jit
def Seff(form, theta):
    return -jnp.log(f(theta) - form(theta) + 0j)

@eqx.filter_jit
@eqx.filter_grad
def Seff_grad(form, theta):
    return jnp.log(f(theta) - form(theta) + 0j).real


opt = optax.yogi(1e-4)
opt_state = opt.init(form)

theta0 = jnp.zeros((V,))
chain = mc.metropolis.Chain(lambda x: Seff(form,x), theta0, chainKey)
while True:
    chain.calibrate()
    phases = []
    for _ in range(300):
        chain.step(N=10)
        act = Seff(form, chain.x)
        phases.append(jnp.exp(1j*act.imag))
        grad = Seff_grad(form, chain.x)
        updates, opt_state = jax.jit(opt.update)(grad, opt_state)
        form = eqx.filter_jit(eqx.apply_updates)(form, updates)
    print(f'{np.mean(phases).real}')

