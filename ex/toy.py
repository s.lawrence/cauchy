#!/usr/bin/env python

# machine learning manifolds for toy model in 0+1
# . ~/env/bin/activate

import numpy as np
import sys
import cmath
import math
import jax
import jax.numpy as jnp
import objax

nbeta = int(sys.argv[1])
nt = int(sys.argv[2])
m = float(sys.argv[3])
g = float(sys.argv[4])
NT = nbeta + 2*nt
Nadj = 1000         # number of sampling in each step of adjustment stepsize for the acceptance rate of 0.3~0.4
Nthm = 1000       # number of steps thermalization
Nskip = 20        # number of steps to skip in MCMC
Nsap = 1000       # number of MCMC samples per training
tr = 5e-3        # training stepsize

# Create toy model
class Model:
	def __init__(self, nbeta, nt, m, g):
		self.nbeta = nbeta
		self.nt = nt
		self.m = m
		self.g = g
		NT = nbeta + 2*nt
		if NT < 1:
			print('need the total number of site to be > 0')
			sys.exit(1)
		self.NT = NT

        # Define the contour.
		M = np.zeros(NT,dtype=np.complex64)
		G = np.zeros(NT,dtype=np.complex64)
		for n in range(0,nbeta):
			M[n] = m
			G[n] = g
		for n in range(nbeta, nbeta+nt):
			M[n] = -1j * m
			G[n] = -1j * g
		for n in range(nbeta+nt, NT):
			M[n] = 1j * m
			G[n] = 1j * g
		self.M = M
		self.G = G

	def action(self, phi):
		M = self.M
		G = self.G
		phi2 = phi*phi
		phi4 = phi2*phi2
		S = phi2 * M + phi4 * G
		S = jnp.sum(S)
		return S




# Map from R to C
class Map(objax.Module):
	def __init__(self, V):
		self.linear_imag = objax.nn.Linear(V, V, True, jnp.zeros)

	def __call__(self, x):
		x = x + 1j*self.linear_imag(x)
		return x

# Defint model and map
map = Map(NT)
model = Model(nbeta, nt, m, g)



@objax.Jit
@objax.Function.with_vars(map.vars())
def Sef(x):
	j = jax.jacfwd(map)(x)
	d = jnp.linalg.det(j)
	xt = map(x)
	Seff = model.action(xt) - jnp.log(d)
	return Seff

# Real part of effective action S - Log Det J
def ReS(x):
	res = Sef(x).real	
	return res

gv = objax.Jit(objax.Grad(ReS, map.vars()))     #Can do just Grad

# MCMC one step
def step(x, d):
	xp = x + np.random.normal(0,d,len(x))
	sdif = ReS(xp) - ReS(x)
	r = jnp.exp(-sdif)
	if(r < 1):
		p = np.random.uniform(0,1)
		if(r < p):
			return x, 0
	return xp, 1

# Adjust delta such that acceptance rate fits 0.3~0.4
def dt_adjust(x, d, Nadj):
	a = 0
	x0 = x
	while(a < 0.3 or a > 0.4):
		if(a < 0.3):
			d = d*0.99
		elif(a > 0.4):
			d = d*1.01

		ac = 0  
		x = therm(x0, d, Nthm)   
		# Estimate current acceptance rate
		for m1 in range(Nadj):
			x, r = step(x,d)
			ac = ac+float(r)/float(Nadj)
			# if(np.mod(m1,100)==0):
			# 	print(ac*float(Nadj)/(m1+1))
		a = ac
		print(d, a)
		if(d > 10):
			a = 0.35

	return d

# Termalization loop
def therm(x, d, Nthm):
	for n in range(Nthm):
		x, r = step(x,d)
	return x

# MCMC sampling for a given Seff. Measures the average of gradients, l is the number of arrays in the list of Map's parameters
def measure(x, d, Nsap, Nskip):
	x = therm(x, d, Nthm)
	sign = []
	gstore = []
	gstore2 = []
	for n in range(Nskip * Nsap):
		x, r = step(x,d)
		if(np.mod(n+1,Nskip)==0):
			imS = Sef(x).imag
			sign.append(np.exp(-1j*imS))
			g1 = gv(x)
			gstore.append(g1[0])
			gstore2.append(g1[1])   

	print('The last state and its ReS: ', x, ReS(x))

	return np.array(sign), np.array(gstore), np.array(gstore2)

def stat(x, Sb, Nb):
	dimb = int(np.size(x,0)/float(Sb))
	xb = x[0:dimb]
	for m in range(dimb):
		xb[m] = np.mean(x[m*Sb:(m+1)*Sb],axis=0)

	bave = x[0:Nb]
	for m1 in range(Nb):
		i = np.random.randint(dimb, size=(dimb))
		xnew = xb[i]
		bave[m1] = np.mean(xnew, axis=0).real
	ave = np.mean(bave, axis=0)
	std = np.std(bave, axis=0)

	return ave, std


# Training
x0 = np.random.normal(0,1.0,NT)
x = x0
dt = 1.0
ave = 0.
std = 0.

for n in range(0,500):
	sign = []
	gstore0 = []
	gstore1 = []
	g = []
	print('-------------- Training number', n+1, '--------------')
	print('* Adjust dt *')
	dt = dt_adjust(x0, dt, Nadj)
	if (dt > 9):
		print('In the wrong homology class, quit')
		break
	print('* Compute gradients and average sign *')
	sign, gstore0, gstore1 = measure(x,dt,Nsap,Nskip)
	print('* Average sign *')
	ave, std = stat(sign, 20, 200)
	print(ave,'+/-',std)
	print('* Compute Gradients and errors *')
	ave0, std0 = stat(np.array(gstore0), 20, 200)
	ave1, std1 = stat(np.array(gstore1), 20, 200)
	print(np.amax(np.abs(ave1)))
	# ave0 = -ave0/(std0+1e-5)
	# ave1 = -ave1/(std1+1e-5)
	ave0 = -ave0
	ave1 = -ave1
	g.append(jnp.array(ave0/np.amax(np.abs(ave0))))
	g.append(jnp.array(ave1/np.amax(np.abs(ave1))))



	if(np.mod(n,20)==0):
		opt = objax.Jit(objax.optimizer.SGD(map.vars()))
		# opt = objax.Jit(objax.optimizer.Adam(map.vars(),beta1=0.9, beta2=0.95))

	opt(tr,g)
	if(np.mod(n,1)==0):
		print('-- Parameters of neural network --') 
		for k, v in map.vars().items():
			print(f'{k}')
			print(f'{v.value}')

