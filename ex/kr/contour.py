import numpy as np
from scipy.optimize import minimize

def linear(p, x):
    a, b = p
    return x + (a+b*x)*1j

def find_contour(b, alpha, gamma):
    p = 0,0
    x = np.linspace(-13,13,2000)

    def quenched(p):
        z = linear(p, x)
        S = b*z + alpha*z**2 + gamma*z**4
        if np.any(S < -100):
            # Return a big value
            return 1e100
        boltz = np.exp(-S)
        return np.trapz(np.abs(boltz * np.gradient(z,x)), x)

    res = minimize(quenched, p, method='Powell')

    p = res.x
    z = linear(p, x)
    S = b*z + alpha*z**2 + gamma*z**4
    Z = np.trapz(np.exp(-S), z)

    return p, np.abs(Z/res.fun)

