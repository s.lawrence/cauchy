#!/usr/bin/env python

import pickle
import sys

import numpy as np

from contour import *

fn = sys.argv[1]
alpha = complex(sys.argv[2])
gamma = complex(sys.argv[3])

br_grid = np.linspace(-8,8,21)
bi_grid = np.linspace(-8,8,21)

contours = [[None for _ in range(len(bi_grid))] for _ in range(len(br_grid))]

for i, br in enumerate(br_grid):
    for j, bi in enumerate(bi_grid):
        b = br + 1j*bi
        cont, sigma = find_contour(b, alpha, gamma)
        #print(f'{alpha} {gamma} {b}   {sigma}  {cont} ({i},{j})')
        #print(f'{alpha} {gamma} {b}   {sigma}  ({i},{j})')
        print(f'{sigma}  ({i},{j}) ({cont})')
        contours[i][j] = cont

with open(fn, 'wb') as f:
    pickle.dump((alpha, gamma, br_grid, bi_grid, contours), f)

