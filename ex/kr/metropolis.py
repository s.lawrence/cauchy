#!/usr/bin/env python

import pickle
import sys

import jax
import jax.numpy as jnp
import numpy as np

from contour import *

# Physical parameters
nbeta = int(sys.argv[1])
nt = int(sys.argv[2])
m2 = float(sys.argv[3])
lamda = float(sys.argv[4])

nbeta1 = int(nbeta/2)
nbeta2 = nbeta-nbeta1

# Load contours
contourtab = {}

for fn in sys.argv[5:]:
    with open(fn, 'rb') as f:
        alpha, gamma, br_grid, bi_grid, contours = pickle.load(f)
        contourtab[alpha,gamma] = contours

# Schwinger-Keldysh contour
s_contour = np.concatenate([
    1j*np.ones(nt),
    np.ones(nbeta1),
    -1j*np.ones(nt),
    np.ones(nbeta2),
    ])

# Reduced action (using the S-contour)
def Sred(zprev):
    t = len(zprev)

    dt_m = s_contour[t-1]
    dt_p = s_contour[t]

    b, alpha, gamma = 0.j, 0.j, 0.j

    # Kinetic piece
    if t > 0:
        b -= zprev[-1]/dt_m
    if t == nbeta + 2*nt - 1:
        b -= zprev[0]/dt_p
    alpha += 0.5/dt_m + 0.5/dt_p

    # Potential piece
    alpha += (m2/2.)*(dt_m + dt_p)/2.
    gamma += (lamda/4.)*(dt_m + dt_p)/2.

    return b, alpha, gamma

# Effective action
def Seff(x):
    zprev = []
    S = 0.
    lndetJ = 0.
    for t, xt in enumerate(x):
        b, alpha, gamma = Sred(zprev)

        # Get nearest contour
        contours = None
        for a, g in contourtab:
            if np.isclose(a,alpha) and np.isclose(g,gamma):
                contours = contourtab[a,g]
        if contours is None:
            raise Exception(f"No contours for {alpha} {gamma}")
        i = min(enumerate(br_grid), key=lambda x: abs(x[1]-b.real))[0]
        j = min(enumerate(bi_grid), key=lambda x: abs(x[1]-b.imag))[0]
        b_ = br_grid[i] + 1j * bi_grid[j]
        p = contours[i][j]

        # Get z
        zt = linear(p, xt)

        # Get dz
        h = 1e-6
        zt_ = linear(p, xt+h)
        dz = (zt_-zt)/h

        # Action
        S += b*zt + alpha*zt**2 + gamma*zt**4
        idealR = (b_*zt + alpha*zt**2 + gamma*zt**4).real
        idealI = (b_*zt + alpha*zt**2 + gamma*zt**4).imag
        if idealR < -3:
            print(f'                                   {idealI}')
            #print('     ', (b_*zt).real, (alpha*zt**2).real)
            #print(b_, alpha, gamma, xt, zt, idealR, '           p: ', p)

        # Jacobian
        lndetJ += np.log(dz)

        zprev.append(zt)

    return S - lndetJ, zprev

# Check that we have all desired contours
missing = []
desired = []
for t in range(nbeta+2*nt):
    zprev = [0.]*t
    _, alpha, gamma = Sred(zprev)
    contours = None
    for a, g in contourtab:
        if np.isclose(a,alpha) and np.isclose(g,gamma):
            contours = contourtab[a,g]
    desired.append((alpha,gamma))
    if contours is None:
        print(f'# Missing {alpha} {gamma}')
        missing.append((alpha,gamma))
if len(missing) > 0:
    print('mkdir -p contourtab')
    for i, (a, g) in enumerate(desired):
        print(f"./tabulate.py contourtab/contours{i}.pickle '{a}' '{g}'")
    sys.exit(0)

# Metropolis
x = np.zeros(nbeta+2*nt)
eps = 0.2 / np.sqrt(nbeta+2*nt)
while True:
    accepted = 0
    proposed = 0
    for _ in range(100):
        xp = x + eps*np.random.normal(size=(nbeta+2*nt,))
        act, z = Seff(x)
        actp, zp = Seff(xp)
        proposed += 1
        if np.random.uniform() < np.exp(act-actp):
            accepted += 1
            x, z, act = xp, zp, actp

    # Print observables
    #print(np.exp(-1j * act.imag), accepted/proposed, *(z[0]*z[t] for t in range(nt+1)))
    print(np.exp(-1j * act.imag), act.real, np.mean(np.abs(z)))

