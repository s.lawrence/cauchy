#!/usr/bin/env python

import sys

import numpy as np

if True:
    R = 5.
    alpha = 1.
    lamda = 1j

    x = np.linspace(-10,10,10000)
    for theta in np.linspace(0, 2*np.pi, 200):
            b = R * np.exp(1j*theta)
            S = b*x + alpha*x**2 + lamda*x**4
            boltz = np.exp(-S)
            Z = np.trapz(boltz, x)
            print(theta, np.log(Z/np.abs(Z)).imag)

if False:
    rl = float(sys.argv[1])
    rh = float(sys.argv[2])
    il = float(sys.argv[3])
    ih = float(sys.argv[4])

    x = np.linspace(-10,10,10000)

    for br in np.linspace(-10,10,100):
        for bi in np.linspace(-10,10,100):
            b = br + 1j*bi
            S = b*x + x**2 + x**4
            boltz = np.exp(-S)
            Z = np.trapz(boltz, x)
            print(np.abs(Z))

