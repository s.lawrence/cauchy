#!/usr/bin/env python

import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np

from contour import *

fn = sys.argv[1]
with open(fn, 'rb') as f:
    alpha, gamma, br_grid, bi_grid, contours = pickle.load(f)

i = 10
j = 11

print(alpha, gamma)

x = np.linspace(-10,10,1000)
z = linear(contours[i][j], x)
plt.figure()
plt.plot(z.real, z.imag)
plt.show()
