#!/usr/bin/env python

import sys

from numpy import *

beta = int(sys.argv[1])
L = int(sys.argv[2])
m = float(sys.argv[3])
g2 = float(sys.argv[4])
mu = float(sys.argv[5])
samples = int(sys.argv[6])
skip = int(sys.argv[7])
therm = int(sys.argv[8])

def idx(t,x):
    return (t%beta)*L + (x%L)

def K(A):
    A = A.reshape((beta,L,2))
    K = m*eye(beta*L) + 0j
    for t in range(beta):
        for x in range(L):
            eta0 = 1
            if t == beta-1:
                eta0 = -1
            eta1 = (-1)**t
            K[idx(t,x),idx(t,x+1)] -= eta1/2 * exp(-1j*A[t,x,1])
            K[idx(t,x+1),idx(t,x)] += eta1/2 * exp(1j*A[t,x,1])
            K[idx(t,x),idx(t+1,x)] -= eta0/2 * exp(-mu - 1j*A[t,x,0])
            K[idx(t+1,x),idx(t,x)] += eta0/2 * exp(mu + 1j*A[t,x,0])
    return K

def action(A):
    return 2./(g2) * sum(1-cos(A)) - log(linalg.det(K(A)))

def observe(A):
    Kinv = linalg.inv(K(A))
    A = A.reshape((beta,L,2))
    dens = 0j
    for t in range(beta):
        for x in range(L):
            eta0 = 1
            if t == beta-1:
                eta0 = -1
            dens += eta0/2 * Kinv[idx(t,x),idx(t+1,x)] * exp(mu + 1j*A[t,x,0])
            dens += eta0/2 * Kinv[idx(t+1,x),idx(t,x)] * exp(-mu - 1j*A[t,x,0])
    return dens / (beta*L)

def sub_obs(obs, f, g, G, gexp):
    return (obs * (f - g) + G*gexp)/(f-g+G)

print(F'# {beta} {L} {m} {g2} {mu}')

# Do a little -jig- Monte Carlo.
A = zeros(2*beta*L)
delta = 1
act = action(A)
for adjusting in [True,False]:
    accepted = 0
    for _ in range(therm):
        for _ in range(skip):
            Ap = A + delta*random.normal(size=len(A)) / sqrt(len(A))
            actp = action(Ap)
            if random.random() < exp((act-actp).real):
                act = actp
                A = Ap
                accepted += 1
        if adjusting:
            if accepted < 0.25 * skip:
                delta *= 0.9
            if accepted > 0.4 * skip:
                delta *= 1.1
            accepted = 0
    if not adjusting:
        print(F'# ACCEPTANCE: {accepted/(therm*skip)}')
for _ in range(samples):
    accepted = 0
    for _ in range(skip):
        Ap = A + delta*random.normal(size=len(A)) / sqrt(len(A))
        actp = action(Ap)
        if random.random() < exp((act-actp).real):
            act = actp
            A = Ap
            accepted += 1
    A = (A+pi) % (2*pi) - pi
    dens = observe(A)
    print(F'{act.imag}   {dens}')


